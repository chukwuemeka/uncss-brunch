"use strict";

const uncss = require("uncss");
const request = require("request-promise-native");
const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const jquery = require("jquery");

class Uncss {
  constructor(config) {
    this.config = config || {};
  }
  optimize(file) {
    if (!this.config || !this.config.plugins || !this.config.plugins.uncss) {
      return Promise.resolve(file.data);
    }
    const options = Object.assign({}, this.config.plugins.uncss.options || {}, {
      raw: file.data,
      report: true
    });
    const urls = this.config.plugins.uncss.urls;
    const promises = urls.map(function(url) {
      return request.get(url).then(function(body) {
        const dom = new JSDOM(body);
        console.log(`Created DOM for ${url}`);
        const $ = jquery(dom.window);
        $("link[rel='stylesheet']").remove();
        $("script").remove();
        return Promise.resolve(dom.serialize());
      });
    });
    return Promise.all(promises).then(function(htmlStrings) {
      return new Promise(function(resolve, reject) {
        console.log(`Raw CSS is ${file.data.length} bytes`);
        uncss(htmlStrings, options, function(error, output, report) {
          if (error) {
            console.error(error);
            reject(error);
          } else {
            const percentChange =
              100 *
              (report.original.length - output.length) /
              report.original.length;
            console.log(`
_________________________________________________
|
|   UnCSS has reduced the file size by ~ ${percentChange.toFixed(1)}%  
|
|________________________________________________
`);
            resolve(output);
          }
        });
      });
    });
  }
}

Uncss.prototype.brunchPlugin = true;
Uncss.prototype.defaultEnv = "*";
Uncss.prototype.type = "stylesheet";
Uncss.prototype.defaultEnv = "production";

module.exports = Uncss;
